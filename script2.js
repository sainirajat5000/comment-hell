const middleContainer = document.querySelector('.middleSection');

const addFreshComment = (commentInput) => {
    const newComment = document.createElement('div');
    newComment.setAttribute('class','parentComment');

    const textContainer = document.createElement('span');
    textContainer.textContent = commentInput;

    const buttonContainer = document.createElement('div');
    buttonContainer.setAttribute('class','commentButtonContainer');

    const deleteButton = document.createElement('button');
    deleteButton.setAttribute('class','deleteButton');
    deleteButton.setAttribute('value','Delete');
    deleteButton.textContent = "Delete";

    const replyButton = document.createElement('button');
    replyButton.setAttribute('class','replyButton');
    replyButton.setAttribute('value','Reply');
    replyButton.textContent = "Reply";

    buttonContainer.appendChild(deleteButton);
    buttonContainer.appendChild(replyButton);
    newComment.appendChild(textContainer);
    newComment.appendChild(buttonContainer);
    return newComment;
}

const deleteThisReply = (parent) => {
    parent.parentNode.removeChild(parent);
}
const createAReplyBox = () => {
    const newElement = document.createElement('div');
    newElement.setAttribute('class','replyPopup');

    const replyBoxInput = document.createElement('input');
    replyBoxInput.setAttribute('type','text');

    const doneButton = document.createElement('button');
    doneButton.setAttribute('value','Done');
    doneButton.textContent = "Done";

    newElement.appendChild(replyBoxInput);
    newElement.appendChild(doneButton);
    return newElement;
}

const replyToThisComment = (parent,itsChild) => {
    const replyBox = createAReplyBox();
    if(itsChild.nextSibling) {
        parent.insertBefore(replyBox,itsChild.nextSibling);
    } else {
        parent.appendChild(replyBox);
    }
}

document.querySelector('.commentBox').addEventListener('click',(e)=>{
    const commentText = e.target.parentNode.querySelector('input').value;
    if(e.target.tagName == "BUTTON") {
        const newComment = addFreshComment(commentText);
        middleContainer.appendChild(newComment);

    }
});

const replyDone = (parent) => {
    const commentText = parent.querySelector('input').value;
    const newComment = addFreshComment(commentText);
    if(parent.nextSibling) {
        parent.parentNode.insertBefore(newComment,parent.nextSibling);
    } else {
        parent.parentNode.appendChild(newComment);
    }
    deleteThisReply(parent);
}

middleContainer.addEventListener('click',(e)=>{
    const buttonClicked = e.target.value;
    if(buttonClicked=='Delete') {
        deleteThisReply(e.target.parentNode.parentNode);
    }
    if(buttonClicked=='Reply') {
        replyToThisComment(e.target.parentNode.parentNode,e.target.parentNode);
    }
    if(buttonClicked=='Done') {
        replyDone(e.target.parentNode);
    }
})

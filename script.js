const commentDone = () => {
  const commentList = document.getElementById("commentContainer");
  const commentContent = document.getElementById("commentInput");

  const newElement = document.createElement("div");
  const spanCreate = document.createElement("span");
  const text = document.createTextNode(commentContent.value);
  const replayButton = document.createElement("button");
  replayButton.textContent = "Reply";
  replayButton.setAttribute("class", "replyButton");
  spanCreate.appendChild(text);
  newElement.appendChild(spanCreate);
  newElement.appendChild(replayButton);
  newElement.setAttribute("class", "firstComment comment");
  commentList.appendChild(newElement);
};
const createReplyBox = () => {
  const newElement = document.createElement("div");
  const inputForText = document.createElement("input");
  inputForText.setAttribute("class", "subReplying");
  const doneButton = document.createElement("button");
  doneButton.setAttribute("class", "doneButton");
  doneButton.textContent = "Done";
  newElement.appendChild(inputForText);
  newElement.appendChild(doneButton);
  return newElement;
};

const handleDoneClick = (inp) => {
  const newElement = document.createElement("div");
  newElement.setAttribute("class", "comment");
  const spanCreate = document.createElement("span");
  const text = document.createTextNode(inp);
  const replayButton = document.createElement("button");
  replayButton.textContent = "Reply";
  replayButton.setAttribute("class", "replyButton");
  spanCreate.appendChild(text);
  newElement.appendChild(spanCreate);
  newElement.appendChild(replayButton);
  return newElement;
};

document.querySelector("#commentContainer").addEventListener("click", (e) => {
  const buttonClicked = e.target;
  const buttonValue = buttonClicked.innerHTML;
  if (buttonValue == "Reply") {
    const replyBox = createReplyBox();

    if (buttonClicked.nextSibling) {
        buttonClicked.parentNode.insertBefore(replyBox, buttonClicked.nextSibling);
    } else {
        buttonClicked.parentNode.appendChild(replyBox);
    }

    buttonClicked.parentNode.insertAdjacentElement(replyBox);
  } else if (buttonValue == "Done") {
    const inputValue = buttonClicked.parentNode.firstChild.value;
    if (inputValue) {
      const replyDone = handleDoneClick(inputValue);

      if (buttonClicked.parentNode.nextSibling) {
        buttonClicked.parentNode.parentNode.insertBefore(replyDone, buttonClicked.parentNode.nextSibling);
      }
      else {
        buttonClicked.parentNode.parentNode.appendChild(replyDone);
      }
      buttonClicked.parentNode.parentNode.removeChild(buttonClicked.parentNode);
    }
  }
});
